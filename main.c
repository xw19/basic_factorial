/*****************************
 * Author: Sourav Moitra
 * Email: sourav@onit.co.io
 * ***************************/


#include <stdio.h>

unsigned long long factorial(int n) {
	unsigned long long fact =  1;
	int i;
	for( i = 1 ; i <= n ; i++)
		fact *= i;
	
			
	return fact;
}


int main() {
	int num;
	printf("Enter number > ");
	scanf("%d", &num);
	long result = factorial(num);
	printf("Result %ld\n", result);
	return 0;
}
